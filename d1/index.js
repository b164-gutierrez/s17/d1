console.log('Hello World');

			// Arrays and Indexes

/*
Store multiple values in a single variable.
	[] ----> Array Literals
*/

				// Sample

let grades = [98.5, 92.2, 93.1, 89.0];
console.log(grades[0]);

				// Alternative wat to write arrays

				// sample

let myTasks = [
	'drink html',
	'eat javacript',
	'inhale css',
	'bake express js'
];


				// Reassigning array values

				// sample #1 (Array before reassignment)

console.log('Array before reassignment');
console.log(myTasks);


				// sample #2 (Array before reassignment)
myTasks[0] = 'hello world';
console.log('Array after reassignment');
console.log(myTasks);
console.log(myTasks[4]);//undefined - (because theres no 4th element)

				// Getting the length of an array

				// sample
let computerBrands = ['acer', 'asus', 'lenovo', 'neo', 'toshiba', ' gateway', 'redfox', 'fujitsu'];
console.log(computerBrands.length);
console.log(computerBrands);

if (computerBrands.length > 5) {
	console.log('We have too many suppliers. Please coordinate with the operations manager.');
}

				// Access the last element of an array
				// sample
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);

				// ARRAY METHODS
				// Mutator MEthods
/*
	functions that 'mutate' or change ad array after they're created.
*/

				// sample
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

				// push() method
/*
	adds an element in the end of an array AND returns array's length syntax:

		arrayName.push();
*/

console.log('Current Array: ');
console.log(fruits);

let fruitsLength = fruits.push('Mango')
console.log(fruitsLength);
console.log('Mutated array from push method: ');
console.log(fruits);

				// Add Elements
// sample
fruits.push('Avocado', 'Guava')
console.log('Mutated array from push method')
console.log(fruits);


				// Pop() Method
/*
	removes the last element in array AND returns the removed element.
	syntax:
			arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);


				// Unshift() Method
/*
	adds one or more elements at the beginning of an array.
	syntax:
			arrayName.unshift('elementA', elementB);
*/

				// sample
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

				// shift()
/*
	removes an element at the beginning of an array AND returns the removed element
		syntax:
				arrayName.shift()
*/
				// sample
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

				// Splice()
/*
	simultaneously removes elements from specified index number and adds elements.
		syntax:

				arrayName.splice(startingIndex, deleteCount, elementsTo BeAdded)
*/

				// sample
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Splice Method');
console.log(fruits);


				// Sort Method
/*
	rearranges the array elements in alphanumeric order.
		syntax:
				arrayName.sort();
*/

				// sample
fruits.sort();
console.log('Sort Method');
console.log(fruits);

				// reverse() Method
/*
	reverses the order of array elements.
		syntax:
				arrayName.reverse();
*/

				// sample
fruits.reverse();
console.log('Reverse Method');
console.log(fruits);


				// NON - MUTATOR METHODS
/*
	- methods that are functions that do not modify or change an array after they'rs created.

	- it returns elements from an array and combining arrays and printing the output.

	- returns the index number of the first matching element found in an array.

	- if no match was found, the result will be -1.

	the search process will be done from the first element proceeding to the last element.

		syntax:
				arrayName.indexOf(searchValue)
				arrayName.indexOf(searchValue, fromIndex)


*/


// sample
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

let firstIndex = countries.indexOf('PH');
console.log(`Result of indexOf method:  ${firstIndex}`);

let invalidCountry = countries.indexOf('BR');
console.log(`indexOf: ${invalidCountry}`);

				// LastIndexOf();
/*
	- returns the index number of the last matching element found in an array.

	- search process is from last element proceeding to the first element.

		syntax:
				arrayName.lastIndexOf(searchValue);

*/

// sample
let lastIndex = countries.lastIndexOf('PH');
console.log(`lastIndexOf method: ${lastIndex}`);

let lastIndexStart = countries.lastIndexOf('PH', 5);
console.log(`lastIndexOf: ${lastIndexStart}`);

				// Slice()
/*
	- portions/slices elements from an array AND returns new array.
		syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
*/

				// sample #1
// slicing off elements from a specified index to the last element.

let slicedArrayA = countries.slice(2);
console.log(countries);  //before .slice
console.log('sliced method:');
console.log(slicedArrayA);  //after .slice


//sample #2 
// slicing off elements starting from a specified index to another index.

let slicedArrayB = countries.slice(2, 4); // (2=[2]CAN, 4=[4th from the first]SG)
console.log(slicedArrayB);

// 
let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

// toString()
/*
	-returns an array as a 'STRING' separated by commas.
*/

// sample
let stringArray = countries.toString();
console.log('toString method');
console.log(stringArray);

// concat();
/*
	combines 2 arrays and returns the combined result.
		syntax:
				arrayA.concat(arrayB);
				arrayA.concat(elementA);
*/

// sample
let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log(`concat method ${tasks}`);
console.log(tasks);


// combining multiple arrays
// sample

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTasks);

// combining arrays with elements
// sample

let combinedtasks = taskArrayA.concat('smell express', 'throw react');
console.log(combinedtasks);

// join() method
/*
	- returns an array as a STRING separated by specified string
		syntax:
				arrayName.join('separatorString');
*/

// sample
let users = ['John', 'Jane', ' Joe', 'Robert'];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));


					// Iteration Methods
/*
	- iteration methods are loops designed to perform repitive tasks on arrays.

	- useful for manipulating array data resulting in complex tasks.
		syntax:
				arrayName.forEach(function(indivElement)){
						statement
				}
*/

					// a. forEach()
/*
	- similar to a for loop that iterates on each array element.
	- normally work with a function supplied as an argument.
*/

// sample
allTasks.forEach(function(task){
	console.log(task)
})


// mini activity

for(i = 0; i < allTasks.length; i++){
	console.log(allTasks[i])
}


// Using forEach with conditional statements
 let filteredTasks = [];
 allTasks.forEach(function(task){
 	if(task.length > 10){
 		filteredTasks.push(task)
 	}
 })
console.log('Result of filtered task:');
console.log(filteredTasks);


// sample for (.push with prompt)

let sampleArray = ['eat', 'drink'];

let data = prompt('add a data');
let firstName = prompt('add firstName:');

sampleArray.push(data);
sampleArray.push(firstName);
console.log(sampleArray);


				// map()
/*
	- it iterates on each element AND return new array with different values depending on the result of the function's operation.

	- useful for performing tasks where mutating/changing the elements are required.

	- unlike forEach method the map method requires the use of a 'return' statement in order to create another array with the performed operation/statement.
			syntax:
					let/const resultArray = arrayName.map(functon(indivElement){
							return statement
					})
*/

				
				// sample
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number
})

console.log('Map Method');
console.log(numbers);
console.log(numberMap);


				// Every()
/*
	- checks if ALL elements in an array meet the given condition.
	- useful for validating data stored in arrays, especially when dealing large amounts of data.
	- returns a true value if all elements meet the condition and false if otherwise.
			syntax:
					let/const resultArray = arrayName.every(function(indivElement){
							return expression/condition
					})
*/

				// sample

let allValid = numbers.every(function(number){
	return (number > 0)
})

console.log('Every Method')
console.log(allValid);


				// Some()
/*
	- checks if atleast ONE element in the array meets the given condition.
	- return a boolean value.
*/

				// sample
let someValid = numbers.some(function(number){
	return (number < 3)
})

console.log('Some Method');
console.log(someValid);

// combining the returned result from the every/some method may be used in other statements (if else) to perform consecutive results.
if(someValid){
	console.log('Some of numbers in the array is greater than 2')
}

if(allValid){
	console.log('All of the numbers in the array is greater than 0')
}




// Filter()
/*
	- returns new array that contains elements which the given condition.
	- returns a empty array if no elements were found.
	- useful for filtering array elements with given condition and shortens the syntax compared to using other array iteration methods.
			syntax:
					let/const resultArray = arrayName.filter(function(indiElement){
						return expression/condtion
					})
*/

// sample
let filtervalid = numbers.filter(function(number){
	return (number < 3)
})
console.log('Filter Method');
console.log(filtervalid);

let nothingFound = numbers.filter(function(number){
	return (number == 0)
})

console.log(nothingFound);




// Filtering using forEach
let filteredNumbers = []

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number)
	}
})
console.log(filteredNumbers)


// sample using filter

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
		return (product.toLowerCase().includes('a')) //chained methods
})
console.log(filteredProducts);

// methods can be 'Chained using them one after another'
// the results of the first method is used on the second method untill all 'chained methods have been resolved'.

// how chaining resolves in our example:
	// 1.
	// 2.


				// Reduce()
/*
	- evaluates elements from left to rigth and returns/reduces the array into single value.

			syntax:
					let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
						return expression/operation
					})
	- accumulator = parameter in the function stores the result for every iteration of the loop.

	- currentValue = the current/next element in the array that is evaluated in each iteration of the loop.				
*/

				// sample
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration' + ++iteration);
	console.log('accumulator' + x);
	console.log('currentValue' + y);

	return x + y
})
console.log('result of reduce method:' + reducedArray);

				// reducing string arrays
				// sample
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y){
	return x + ' ' + y;
})
console.log('Reduced Method')
console.log(reducedJoin);


// Multidimensional Arrays
// for complex data structures

// how to access multi dimensional arrays
let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5']
]
console.log(chessBoard);
console.log(chessBoard[0][2]);

console.log('Pawn moves to:' + chessBoard[1][5]);

